#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal.h>

#define ONE_WIRE_BUS 13

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress* tablicaAdresow;

byte liczbaCzujnikow;
float measurment;
float averageTemp;

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs,en,d4,d5,d6,d7);

void setSensors()
{
   Serial.begin(9600);
   Serial.println("Stacja meterologiczna konsola");

   Serial.println("Szukanie czujnikow ...");
   sensors.begin();
   Serial.print("Znaleziono ");
   liczbaCzujnikow = sensors.getDeviceCount();
   Serial.print(liczbaCzujnikow, DEC);
   Serial.println(" czujnikow.");
   tablicaAdresow = new DeviceAddress[liczbaCzujnikow];
  for(byte i = 0;i<liczbaCzujnikow;i++)
  {
    sensors.getAddress(tablicaAdresow[i],i);
  }
  printAdresses();
}//setSensors

void setLcd()
{
  lcd.begin(16,2);
  lcd.print("Stacja pomiaru");
  lcd.setCursor(0,1);
  lcd.print("temperatury");
  delay(3000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Znaleziono:");
  lcd.setCursor(0,1);
  lcd.print(liczbaCzujnikow);
  lcd.print(" czujnikow");
  delay(3500);
  lcd.clear();
}
void setup() {
  delay(1000);
  setSensors();
  setLcd();
}//setup

void readTemp()
{ 
  averageTemp = 0;
  for(byte i = 0;i<liczbaCzujnikow;i++)
  {

    Serial.print("Odczyt z czujnika nr ");
    Serial.print(i+1);
    measurment = sensors.getTempC(tablicaAdresow[i]);
    Serial.print(" Temp C ");
    Serial.print(measurment);
    Serial.println();
    averageTemp += measurment;
  }
  averageTemp = averageTemp / liczbaCzujnikow;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Srednia temp ");
  lcd.setCursor(0,1);
  lcd.print(averageTemp);
  lcd.print(" C");
}
void loop() {
 Serial.print("Requesting temperatures...");
 sensors.requestTemperatures();
 Serial.println("DONE");
 readTemp();
 

  delay(3000);
}//loop

void printAdresses()
{
  for(int j = 0;j<liczbaCzujnikow;j++)
  {
    Serial.print("Czujnik nr ");
    Serial.print(j);
    for (uint8_t i = 0; i < 8; i++)
    {
     if (tablicaAdresow[j][i] < 16) Serial.print("0");
      Serial.print(tablicaAdresow[j][i], HEX);
    }
    Serial.println();
  }
}

