#include<LiquidCrystal.h>

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#include <OneWire.h>
#include <DallasTemperature.h>

const int rs = 13, en = 12, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
# define SCROOL true
LiquidCrystal lcd(rs,en,d4,d5,d6,d7);

#define DHTPIN 6
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);


#define ONE_WIRE_BUS 1 //szyna danych DS18B20
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress* tablicaAdresow;
byte liczbaCzujnikow;
void printText(String line1 = "", String line2 = "", bool scrollLine1=false, bool scrollLine2=false);

const byte degree[8] = {
  B01100,
  B10010,
  B10010,
  B01100,
  B00000,
  B00000,
  B00000,
  B00000
};

const byte teer[] = {
  B00000,
  B00100,
  B01100,
  B01110,
  B11111,
  B11111,
  B11111,
  B01110
};
const byte tempSymbol[] = {
  B00110,
  B00100,
  B00110,
  B00100,
  B00110,
  B00100,
  B01010,
  B01110
};
void setup() 
{
 
  String s1 = "Stacja meteorologiczna";
  String s2 = "Wykrywanie czujnikow temperatory";
  //while (!Serial) ;
  initUSB(s1,s2);
  initLCD(s1,s2);
  detectingSensors();
  sensors.requestTemperatures();
  dht.readHumidity();
}

void loop() 
{
  readTempandHum();

}

void initUSB(String s1,String s2)
{
  Serial.begin(9600);
  Serial.println(s1);
  Serial.print(s2);
  Serial.println("...");
}
void initLCD(String s1,String s2)
{
  lcd.createChar(0,degree);
  lcd.createChar(1,teer);
  lcd.createChar(2,tempSymbol);
  lcd.begin(16,2);
  printText(s1.substring(0,6),s1.substring(7));
  delay(3000);
  lcd.clear();
}
void detectingSensors()
{
  //DS18B20 sensors
  sensors.begin();
  lcd.setCursor(0,0);
  liczbaCzujnikow = sensors.getDeviceCount();
  String text = "";
  text = text+"Znaleziono "+liczbaCzujnikow+" czujnikow temp.";
  printText(text.substring(0,12),text.substring(13));
  tablicaAdresow = new DeviceAddress[liczbaCzujnikow];
  for(byte i = 0;i<liczbaCzujnikow;i++)
  {
    sensors.getAddress(tablicaAdresow[i],i);
  }
  
  //DHT11 sensors
  dht.begin();
  
}
void printMeasurments(float temp, float hum)
{
    lcd.clear();
    String tempText = "Sr temp  ";
    tempText +=temp;
    tempText +=" C";
    String humText = "Sr wilg  ";
    humText +=hum;
    humText +=" %";

    lcd.setCursor(0,0);
    lcd.print(tempText);
    lcd.setCursor(8,0);
    lcd.write((byte)2);
    lcd.setCursor(14,0);
    lcd.write((byte)0);
    lcd.setCursor(0,1);
    lcd.print(humText);
    lcd.setCursor(8,1);
    lcd.write((byte)1);
    
    Serial.println(tempText);
    Serial.println(humText);
    delay(2000);
 
}
void printText(String line1 = "", String line2 = "", bool scrollLine1=false, bool scrollLine2=false)
{

  if(line1.length()!=0 && line2.length()==0  && scrollLine1==false && scrollLine2==false)
  {
    lcd.setCursor(0,0);
    lcd.print(line1);

  }
  else if(line1.length()!=0 && line2.length()!=0  && scrollLine1==false && scrollLine2==false)
  {
    lcd.setCursor(0,0);
    lcd.print(line1);
    lcd.setCursor(0,1);
    lcd.print(line2);
  }
  else
  {
    lcd.setCursor(0,0);
    lcd.print("Blad");
  }
}
void readTempandHum()
{ 
  float hum=0;;
  hum=dht.readHumidity();
  if (isnan(hum))
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  
  float averageTemp=0;
  float measurment;
  for(byte i = 0;i<liczbaCzujnikow;i++)
  {
    measurment = sensors.getTempC(tablicaAdresow[i]);
    averageTemp += measurment;
  }
  
  averageTemp = averageTemp / liczbaCzujnikow;

  sensors.requestTemperatures();
  dht.readHumidity();
  printMeasurments(averageTemp,hum);
}
